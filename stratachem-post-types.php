<?php

/*
Plugin Name: Stratachem Post Types
*/
  function stratachem_post_types() {
    register_post_type('use-case', array(
      'supports' => array('title', 'editor', 'thumbnail'),
      'rewrite' => array('slug' => 'use-cases'),
      'public' => true,
      'has_archive' => true,
      'labels' => array(
        'name' => 'Use Cases',
        'add_new_item' => 'Add New Use Case',
        'edit_item' => 'Edit Use Case',
        'all_items' => 'All Use Cases',
        'singular_name' => 'Use Case'
      ),
      'menu_icon' => 'dashicons-tag',
      'show_in_nav_menus' => true,
      'hierarchical', true,
    ));
  };
  add_action('init', 'stratachem_post_types');

?>
